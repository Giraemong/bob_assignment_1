#include "sum.h"

int __sum__(int num)
{
	int res =0;

	for( int i = 0; i < num; i++)
	{
		res += i;
	}

	return res;
}
