CC = g++
TARGET = sum
OBJECT = sum.o main.o

all : sum

$(TARGET) : $(OBJECT)
	$(CC) -o $(TARGET) sum.o main.o

sum.o : sum.h sum.cpp
	$(CC) -c -o sum.o sum.cpp

main.o : sum.h main.cpp
	$(CC) -c -o main.o main.cpp

clean:
	rm -f *.o
	rm -f sum
